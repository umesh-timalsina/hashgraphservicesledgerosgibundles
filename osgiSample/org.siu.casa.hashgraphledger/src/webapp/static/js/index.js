var inspectBtn = document.querySelector('#inspectBtn');

inspectBtn.addEventListener("click", function(){
	var count = 1;
	 $.ajax({
	         url: '/rest/viewservices',
	         method: 'GET',
	         data: {
	         },
	         success: function(data) { 
	        	 var trHTML = '';
	        	 
	             $.each(data, function (i, item) {
	                 $("#servicesBody").append( '<tr><td>' + count  + '</td><td>' + i + '</td><td>' + item + '</td><td><button class="btn btn-primary">Use Service</button></td>' + '</tr> ');
	             });
	             count += 1;
	         },
	         error: function(a,b,c){alert(c);}
		 });
}); 