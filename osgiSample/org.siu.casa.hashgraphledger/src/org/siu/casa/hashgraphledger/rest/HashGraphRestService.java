package org.siu.casa.hashgraphledger.rest;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.http.whiteboard.propertytypes.HttpWhiteboardResource;
import org.osgi.service.jaxrs.whiteboard.propertytypes.JaxrsResource;

import com.google.gson.Gson;


@Component(service=HashGraphRestService.class)
@JaxrsResource
@HttpWhiteboardResource(pattern="/static/*", prefix="static")
public class HashGraphRestService {

	private Gson gson;
	@Activate
	void init() {
		this.gson = new Gson();
	}
	@GET
	@Path("/rest/viewservices")
	@Produces("application/json")
	public String viewAvailableServices(){
		System.out.println("Been Here...");
		Map<String, String> availableServices = new HashMap<>();
		availableServices.put("Open Door", "1.5");
		availableServices.put("Turn On Led", "1.0");
		return gson.toJson(availableServices);
	}
	
}
