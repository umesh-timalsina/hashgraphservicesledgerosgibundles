package org.siu.casa.hashgraphledger.remoteservice.api;

import java.util.Map;

public interface IRaspberrypiService {
	public Map<String, String> viewAvailableServices(); 
	public boolean useService();
}
